# T-Slices: Trusted deep learning inference at the untrusted edge

This repository contains the source code for the T-Slices project. 
For more information, please read our paper [Confidential Execution of Deep Learning Inference at the Untrusted Edge with ARM TrustZone](https://dl.acm.org/doi/abs/10.1145/3577923.3583648).

The implementation is based on [Darknet](https://github.com/pjreddie/darknet) and [Darknetz](https://github.com/mofanv/darknetz).

## Testing Environment

The following environments were used to perform the experiments for this project:

* Ubuntu 18.04.4 with Linux 4.15.0-generic
* OP-TEE v3.10
* QEMU v8

### Devices used for test cases
- Raspberry Pi 3B
- STM32MP157C-DK2

## Installation of OP-TEE
- Install OP-TEE (Rpi3/STM32/Qemu). For more information, see [OP-TEE documentation](https://optee.readthedocs.io/en/latest).
- Build following the OP-TEE documentation:
    - [Rpi3 OP-TEE documentation](https://optee.readthedocs.io/en/latest/building/devices/rpi3.html).
    - [STM32MP1 OP-TEE documentation](https://optee.readthedocs.io/en/latest/building/devices/stm32mp1.html).
    - [Qemu OP-TEE documentation](https://optee.readthedocs.io/en/latest/building/devices/qemu.html#qemu-v8).

## Building T-Slices
- Clone this repo in the `<optee_project_dir>/optee_examples` folder.
- Make necessary changes to library paths in CMakeLists.txt
- Copy necessary data files to `<optee_project_dir>/out-br/target/root/` folder. 
  - Example files: *cfg files, weight files, data files*.
- Build and flash the device following the OP-TEE documentation.

## Running T-Slices
- Login with password `root`
- To run the app, use command: `optee_example_tslices classifier predict <path-to-data-labels> <path-to-cfg> <path-to-weights> <path-to-test-image-files>`
- See the `classifier.c` for more information on other arguments.

### Example Run
- Prediction with Alexnet:
  - Copy files in the `datafiles` folder to `<optee_project_dir>/out-br/target/root/` folder.
  - Then run t-slices with argument `-sl 1`:
    - `optee_example_tslices classifier predict cfg/imagenet1k.data cfg/alexnet.cfg weights/alexnet.weights data/eagle.jpg -sl 1`
- Note that, to change the TA heap memory size, update `TA_DATA_SIZE` in `user_ta_header_defines.h`.

## Troubleshooting
