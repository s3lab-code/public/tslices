#ifndef IM2COL_TA_H
#define IM2COL_TA_H

void im2col_cpu_TA(float* data_im,
                int channels, int height, int width,
                int ksize, int stride, int pad, float* data_col);

void im2col_cpu_TA_per_channel(float* data_im,
                               int channels,  int height,  int width,
                               int ksize,  int stride, int pad, float* data_col, int data_im_offset);

#endif
