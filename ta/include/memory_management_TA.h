
#ifndef MEMORY_MANAGEMENT_TA_H
#define MEMORY_MANAGEMENT_TA_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "darknet_TA.h"

void allocate_memory_for_layer(layer_TA *lta);
void deallocate_memory_for_layer(layer_TA *lta);


int ss_write_object(char *obj_id, size_t obj_id_len, float *data, size_t data_len);
int ss_read_object(char *obj_id, size_t obj_id_len, float *data, size_t data_len);

#endif //MEMORY_MANAGEMENT_TA_H
