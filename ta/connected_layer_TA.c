#include <stdio.h>
#include <time.h>
#include <assert.h>

#include "utils_TA.h"
#include "darknet_TA.h"
#include "gemm_TA.h"
#include "math_TA.h"
#include "blas_TA.h"
#include "activations_TA.h"
#include "batchnorm_layer_TA.h"
#include "connected_layer_TA.h"
#include "convolutional_layer_TA.h"

#include <tee_internal_api.h>
#include <tee_internal_api_extensions.h>
#include <utee_defines.h>

void forward_connected_layer_TA_new(layer_TA *l, network_TA *net)
{
    //DMSG("has been called");
    if (net->is_debug) printf("***(connected_layer): l.channel_index=%d, l.groups=%d, l.batch=%d, l.size=%d, l.n=%d, l.c=%d, l.h=%d, l.w=%d, l.out_h=%d, l.out_w=%d, l.nweights=%d, l.outputs=%d, l.inputs=%d\n", l->channel_index, l->groups, l->batch, l->size, l->n, l->c, l->h, l->w, l->out_h, l->out_w, l->nweights, l->outputs, l->inputs);

    if (net->train || l->num_merged_sublayers == -1){
        /* no slicing */
        int m = l->batch;
        int k = l->inputs;
        int n = l->outputs;
        float *a = net->input;
        float *b = l->weights;
        float *c = l->output;
        gemm_TA(0,1,m,n,k,1,a,k,b,k,1,c,n);

        if(l->batch_normalize){
            forward_batchnorm_layer_TA(l, net);
        } else {
            add_bias_TA(l->output, l->biases, l->batch, l->outputs, 1);
        }
        activate_array_TA(l->output, l->outputs*l->batch, l->activation);
    }
    else{
        /* with slicing */
        int m = l->batch;
        int k = l->weight_size_per_channel; //l.weight_size_per_channel = l.inputs
        int n1 = l->num_merged_sublayers;
        int n2 = l->channel_index;
        float *a = net->input;
        float *b = l->weights;
        float *c = l->output;

        TEE_Time time1, time2, result;

        //benchmark for GEMM
        if (net->is_benchmark){
            TEE_GetSystemTime(&time1);
        }
        gemm_nt_TA_per_channel(m,n1,k,1,a,k,b,k,c,n2);
        if (net->is_benchmark){
            TEE_GetSystemTime(&time2);
            TEE_TIME_SUB(time2, time1, result);
            *net->gemm_time += result.seconds*1000 + result.millis;
        }

        if(l->channel_index == l->outputs){
            if(l->batch_normalize){
                forward_batchnorm_layer_TA(l, net);
            } else {
                add_bias_TA(l->output, l->biases, l->batch, l->outputs, 1);
            }
            activate_array_TA(l->output, l->outputs*l->batch, l->activation);
        }
    }
}

void backward_connected_layer_TA_new(layer_TA l, network_TA net)
{
    gradient_array_TA(l.output, l.outputs*l.batch, l.activation, l.delta);

    if(l.batch_normalize){
        backward_batchnorm_layer_TA(l, net);
    } else {
        backward_bias_TA(l.bias_updates, l.delta, l.batch, l.outputs, 1);
    }

    int m = l.outputs;
    int k = l.batch;
    int n = l.inputs;
    float *a = l.delta;
    float *b = net.input;
    float *c = l.weight_updates;

    gemm_TA(1,0,m,n,k,1,a,m,b,n,1,c,n);

    //diff_private_SGD(l.bias_updates, l.outputs);
    //diff_private_SGD(l.weight_updates, l.inputs*l.outputs);

    m = l.batch;
    k = l.outputs;
    n = l.inputs;

    a = l.delta;
    b = l.weights;
    c = net.delta;

    if(c)gemm_TA(0,0,m,n,k,1,a,k,b,n,1,c,n);
}


void update_connected_layer_TA_new(layer_TA l, update_args_TA a)
{
    float learning_rate = a.learning_rate*l.learning_rate_scale;
    float momentum = a.momentum;
    float decay = a.decay;
    int batch = a.batch;

    axpy_cpu_TA(l.outputs, learning_rate/batch, l.bias_updates, 1, l.biases, 1);

    scal_cpu_TA(l.outputs, momentum, l.bias_updates, 1);

    if(l.batch_normalize){
        axpy_cpu_TA(l.outputs, learning_rate/batch, l.scale_updates, 1, l.scales, 1);
        scal_cpu_TA(l.outputs, momentum, l.scale_updates, 1);
    }

    axpy_cpu_TA(l.inputs*l.outputs, -decay*batch, l.weights, 1, l.weight_updates, 1);
    axpy_cpu_TA(l.inputs*l.outputs, learning_rate/batch, l.weight_updates, 1, l.weights, 1);
    scal_cpu_TA(l.inputs*l.outputs, momentum, l.weight_updates, 1);
}


void allocate_memory_connected_layer(layer_TA *l, network_TA *net){
    if (net->is_debug) printf("allocate_memory_connected_layer\n");
    int i;

    // only for inference purpose
    l->sublayer_size = 0;
    l->sublayer_size += l->inputs; //input
    l->sublayer_size += l->batch*l->outputs; //output
    l->sublayer_size += l->weight_size_per_channel; //weight
    l->sublayer_size += l->outputs; //bias
    //if(net->adam) l->sublayer_size += 4*l->outputs + 2*l->nweights;
    if(l->batch_normalize) l->sublayer_size += 3*l->outputs;
    l->layer_size = l->sublayer_size - l->weight_size_per_channel + l->nweights;
    if (net->is_debug) printf("l.sublayer_size = %d, l.layer_size=%d\n", l->sublayer_size, l->layer_size);

    // Note: for inference, l.weights, l.biases, l.scales, l.rolling_mean, l.rolling_variance is allocated in parser_TA
    l->output = calloc(l->batch*l->outputs, sizeof(float));
    fill_cpu_TA(l->outputs*l->batch, 0, l->output, 1);

    if (net->train){
        l->weights = calloc(l->outputs*l->inputs, sizeof(float));
        //float scale = 1./sqrt(l.inputs);
        float scale = ta_sqrt(2./l->inputs);
        for(i = 0; i < l->outputs*l->inputs; ++i){
            //l.weight_updates[i] = 1.0f;
            //l.weights[i] = scale * rand_uniform_TA(-1, 1);
            l->weights[i] = scale * rand_uniform_TA(-1, 1);
        }
        l->weight_updates = calloc(l->nweights, sizeof(float));

        l->biases = calloc(l->outputs, sizeof(float));
        l->bias_updates = calloc(l->outputs, sizeof(float));
        fill_cpu_TA(l->outputs, 0, l->biases, 1);

        l->delta = calloc(l->batch*l->outputs, sizeof(float));
        fill_cpu_TA(l->batch*l->outputs, 0, l->delta, 1);

        if(net->adam){
            l->m = calloc(l->nweights, sizeof(float));
            l->v = calloc(l->nweights, sizeof(float));
            l->bias_m = calloc(l->outputs, sizeof(float));
            l->scale_m = calloc(l->outputs, sizeof(float));
            l->bias_v = calloc(l->outputs, sizeof(float));
            l->scale_v = calloc(l->outputs, sizeof(float));
        }

        if(l->batch_normalize){
            l->scales = calloc(l->outputs, sizeof(float));
            l->rolling_mean = calloc(l->outputs, sizeof(float));
            l->rolling_variance = calloc(l->outputs, sizeof(float));

            l->scale_updates = calloc(l->outputs, sizeof(float));
            for(i = 0; i < l->outputs; ++i) l->scales[i] = 1;

            l->mean = calloc(l->outputs, sizeof(float));
            l->mean_delta = calloc(l->outputs, sizeof(float));
            l->variance = calloc(l->outputs, sizeof(float));
            l->variance_delta = calloc(l->outputs, sizeof(float));

            l->x = calloc(l->batch*l->outputs, sizeof(float));
            l->x_norm = calloc(l->batch*l->outputs, sizeof(float));
        }
    }
}

void deallocate_memory_connected_layer(layer_TA *l, network_TA *net){
    if (net->is_debug) printf("deallocate_memory_connected_layer\n");

    //note: l.output is freed in network_TA

    free(l->weights);
    free(l->biases);

    if (net->train){
        free(l->weight_updates);
        free(l->bias_updates);
        free(l->delta);
    }

    if(net->adam){
        free(l->m);
        free(l->v);
        free(l->bias_m);
        free(l->scale_m);
        free(l->bias_v);
        free(l->scale_v);
    }

    if(l->batch_normalize){
        free(l->scales);
        free(l->scale_updates);
        free(l->mean);
        free(l->variance);
        free(l->mean_delta);
        free(l->variance_delta);
        free(l->rolling_mean);
        free(l->rolling_variance);
        free(l->x);
        free(l->x_norm);
    }
}


layer_TA make_connected_layer_TA_new(int batch, int inputs, int outputs, ACTIVATION_TA activation, int batch_normalize, int adam, network_TA *net)
{
    int i;
    layer_TA l = {0};
    l.learning_rate_scale = 1;
    l.type = CONNECTED_TA;

    l.inputs = inputs;
    l.outputs = outputs;
    l.batch = batch;
    l.batch_normalize = batch_normalize;
    l.h = 1;
    l.w = 1;
    l.c = inputs;
    l.out_h = 1;
    l.out_w = 1;
    l.out_c = outputs;
    l.nweights = l.inputs * l.outputs;

    l.forward_TA = forward_connected_layer_TA_new;
    l.backward_TA = backward_connected_layer_TA_new;
    l.update_TA = update_connected_layer_TA_new;

    l.activation = activation;

    l.weight_size_per_channel = l.inputs;
    l.num_merged_sublayers = -1;

    if(net->train)
        allocate_memory_connected_layer(&l, net);

    //IMSG("connected_TA                         %4d  ->  %4d\n", inputs, outputs);

    return l;
}

