#include "blas_TA.h"
#include "math_TA.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void fill_cpu_TA(int N, float ALPHA, float *X, int INCX)
{
    int i;
    for(i = 0; i < N; ++i) X[i*INCX] = ALPHA;
}

void fill_cpu_TA_new(int N, float ALPHA, float *X, int INCX, int kernel_number, int output_size)
{
    int i;
    for(i = 0; i < N; ++i) X[kernel_number*output_size + i*INCX] = ALPHA;
}

void copy_cpu_TA(int N, float *X, int INCX, float *Y, int INCY)
{
    int i;
    for(i = 0; i < N; ++i) {
        Y[i*INCY] = X[i*INCX];
    }
}

void mean_cpu_TA(float *x, int batch, int filters, int spatial, float *mean)
{
    float scale = 1./(batch * spatial);
    int i,j,k;
    for(i = 0; i < filters; ++i){
        mean[i] = 0;
        for(j = 0; j < batch; ++j){
            for(k = 0; k < spatial; ++k){
                int index = j*filters*spatial + i*spatial + k;
                mean[i] += x[index];
            }
        }
        mean[i] *= scale;
    }
}

void variance_cpu_TA(float *x, float *mean, int batch, int filters, int spatial, float *variance)
{
    float scale = 1./(batch * spatial - 1);
    int i,j,k;
    for(i = 0; i < filters; ++i){
        variance[i] = 0;
        for(j = 0; j < batch; ++j){
            for(k = 0; k < spatial; ++k){
                int index = j*filters*spatial + i*spatial + k;
                variance[i] += ta_pow((x[index] - mean[i]), 2);
            }
        }
        variance[i] *= scale;
    }
}

void scal_cpu_TA(int N, float ALPHA, float *X, int INCX)
{
    int i;
    for(i = 0; i < N; ++i) X[i*INCX] *= ALPHA;
}

void axpy_cpu_TA(int N, float ALPHA, float *X, int INCX, float *Y, int INCY)
{
    int i;
    for(i = 0; i < N; ++i) Y[i*INCY] += ALPHA*X[i*INCX];
}


void normalize_cpu_TA(float *x, float *mean, float *variance, int batch, int filters, int spatial)
{
    int b, f, i;
    for(b = 0; b < batch; ++b){
        for(f = 0; f < filters; ++f){
            for(i = 0; i < spatial; ++i){
                int index = b*filters*spatial + f*spatial + i;
                x[index] = (x[index] - mean[f])/(ta_sqrt(variance[f]) + .000001f);
            }
        }
    }
}


void softmax_TA(float *input, int n, float temp, int stride, float *output)
{
    int i;
    float sum = 0;
    float largest = -first_aim_money;

    char temp_str_input[10];
    char temp_str_sum[10];
    int is_pos = 0;

    for(i = 0; i < n; ++i){
        if(input[i*stride] > largest) largest = input[i*stride];

        if (0){
            is_pos = (input[i*stride] > 0) ? 1: 0;
            ftoa(input[i], temp_str_input, 3);
            printf("softmax_TA: i=%d, input = %s, is_pos=%d\n", i, temp_str_input, is_pos);
        }
    }


    for(i = 0; i < n; ++i){
        float e_ta = ta_exp(input[i*stride]/temp - largest/temp);
        sum += e_ta;
        output[i*stride] = e_ta;

        if (0){
            is_pos = (e_ta > 0) ? 1: 0;
            ftoa(e_ta, temp_str_input, 5);
            ftoa(sum, temp_str_sum, 5);
            printf("softmax_TA: i=%d, e_ta=%s, sum=%s, is_pos=%d\n", i, temp_str_input, temp_str_sum, is_pos);
        }
    }

    for(i = 0; i < n; ++i){
        output[i*stride] /= sum;

        if (0){
            ftoa(output[i*stride], temp_str_input, 3);
            printf("softmax_TA: i=%d, output = %s\n", i, temp_str_input);
        }
    }
}


void softmax_cpu_TA(float *input, int n, int batch, int batch_offset, int groups, int group_offset, int stride, float temp, float *output)
{
    int g, b;
    for(b = 0; b < batch; ++b){
        for(g = 0; g < groups; ++g){
            softmax_TA(input + b*batch_offset + g*group_offset, n, temp, stride, output + b*batch_offset + g*group_offset);
        }
    }
}



void softmax_x_ent_cpu_TA(int n, float *pred, float *truth, float *delta, float *error)
{
    int i;
    for(i = 0; i < n; ++i){
        float t = truth[i];
        float p = pred[i];
        error[i] = (t) ? -ta_ln(p) : 0;
        delta[i] = t-p;
    }
}



void smooth_l1_cpu_TA(int n, float *pred, float *truth, float *delta, float *error)
{
    int i;
    for(i = 0; i < n; ++i){
        float diff = truth[i] - pred[i];
        float abs_val = fabs(diff);
        if(abs_val < 1) {
            error[i] = diff * diff;
            delta[i] = diff;
        }
        else {
            error[i] = 2*abs_val - 1;
            delta[i] = (diff < 0) ? 1 : -1;
        }
    }
}


void l1_cpu_TA(int n, float *pred, float *truth, float *delta, float *error)
{
    int i;
    for(i = 0; i < n; ++i){
        float diff = truth[i] - pred[i];
        error[i] = fabs(diff);
        delta[i] = diff > 0 ? 1 : -1;
    }
}

void l2_cpu_TA(int n, float *pred, float *truth, float *delta, float *error)
{
    int i;
    for(i = 0; i < n; ++i){
        float diff = truth[i] - pred[i];
        error[i] = diff * diff;
        delta[i] = diff;
    }
}
