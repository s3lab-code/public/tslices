
#include <tee_internal_api.h>
#include <tee_internal_api_extensions.h>

#include "memory_management_TA.h"
#include "network_TA.h"

#include "convolutional_layer_TA.h"
#include "maxpool_layer_TA.h"
#include "dropout_layer_TA.h"
#include "connected_layer_TA.h"
#include "softmax_layer_TA.h"
#include "avgpool_layer_TA.h"
#include "cost_layer_TA.h"

void allocate_memory_for_layer(layer_TA *lta){
    switch (lta->type){
        case CONVOLUTIONAL_TA: {
            allocate_memory_convolutional_layer(lta, &netta);
            break;
        }
        case CONNECTED_TA: {
            allocate_memory_connected_layer(lta, &netta);
            break;
        }
        case MAXPOOL_TA:{
            allocate_memory_maxpool_layer(lta, &netta);
            break;
        }
        case AVGPOOL_TA:{
            allocate_memory_avgpool_layer(lta, &netta);
            break;
        }
        default:
            break;
    }
}

void deallocate_memory_for_layer(layer_TA *lta){
    switch (lta->type){
        case CONVOLUTIONAL_TA: {
            deallocate_memory_convolutional_layer(lta, &netta);
            break;
        }
        case CONNECTED_TA: {
            deallocate_memory_connected_layer(lta, &netta);
            break;
        }
        case MAXPOOL_TA:{
            deallocate_memory_maxpool_layer(lta, &netta);
            break;
        }
        case AVGPOOL_TA:{
            deallocate_memory_avgpool_layer(lta, &netta);
            break;
        }
        default:
            break;
    }
}


int ss_write_object(char *obj_id, size_t obj_id_len, float *data, size_t data_len){
    TEE_ObjectHandle object;
    TEE_Result res;
    uint32_t obj_data_flag;

    char *id = TEE_Malloc(obj_id_len, 0);
    if (!id){
        EMSG("memory_management_TA:: TEE_ERROR_OUT_OF_MEMORY");
        return 0;
    }

    TEE_MemMove(id, obj_id, obj_id_len);
    //printf("object id=%s, len=%d\n", id, obj_id_len);

    float *vec = TEE_Malloc(data_len, 0);
    if (!vec){
        EMSG("memory_management_TA:: TEE_ERROR_OUT_OF_MEMORY");
        return 0;
    }

    TEE_MemMove(vec, data, data_len);

    /*
     * Create object in secure storage and fill with data
     */
    obj_data_flag = TEE_DATA_FLAG_ACCESS_READ |		/* we can later read the oject */
                    TEE_DATA_FLAG_ACCESS_WRITE |		/* we can later write into the object */
                    TEE_DATA_FLAG_ACCESS_WRITE_META |	/* we can later destroy or rename the object */
                    TEE_DATA_FLAG_OVERWRITE;		/* destroy existing object of same ID */

    res = TEE_CreatePersistentObject(TEE_STORAGE_PRIVATE,
                                     id, obj_id_len,
                                     obj_data_flag,
                                     TEE_HANDLE_NULL,
                                     NULL, 0,		/* we may not fill it right now */
                                     &object);

    if (res != TEE_SUCCESS) {
        EMSG("TEE_CreatePersistentObject failed 0x%08x", res);
        TEE_Free(id);
        TEE_Free(vec);
        return 0;
    } else{
        //IMSG("TEE_CreatePersistentObject done!");
    }

    res = TEE_WriteObjectData(object, vec, data_len);
    if (res != TEE_SUCCESS) {
        EMSG("TEE_WriteObjectData failed 0x%08x", res);
        TEE_CloseAndDeletePersistentObject1(object);
    } else {
        //IMSG("TEE_WriteObjectData done!");
        TEE_CloseObject(object);
    }
    TEE_Free(id);
    TEE_Free(vec);
    return res != TEE_SUCCESS? 0 : 1;
}


int ss_read_object(char *obj_id, size_t obj_id_len, float *data, size_t data_len){
    TEE_ObjectHandle object;
    TEE_ObjectInfo object_info;
    TEE_Result res;
    uint32_t read_bytes;

    /*
     * Check the object exist and can be dumped into output buffer
     * then dump it.
     */
    char *id = TEE_Malloc(obj_id_len, 0);
    if (!id){
        EMSG("memory_management_TA:: TEE_ERROR_OUT_OF_MEMORY");
        return 0;
    }
    TEE_MemMove(id, obj_id, obj_id_len);
    //printf("object id=%s, len=%d\n", id, obj_id_len);

    //printf("calling TEE_OpenPersistentObject\n");
    res = TEE_OpenPersistentObject(TEE_STORAGE_PRIVATE,
                                   id, obj_id_len,
                                   TEE_DATA_FLAG_ACCESS_READ |
                                   TEE_DATA_FLAG_SHARE_READ,
                                   &object);
    if (res != TEE_SUCCESS) {
        EMSG("Failed to open persistent object, res=0x%08x", res);
        return 0;
    }

    //printf("calling TEE_GetObjectInfo1\n");
    res = TEE_GetObjectInfo1(object, &object_info);
    if (res != TEE_SUCCESS) {
        EMSG("Failed to create persistent object, res=0x%08x", res);
        TEE_CloseObject(object);
        TEE_Free(id);
        return 0;
    }

    //printf("checking object_info\n");
    if (object_info.dataSize > data_len) {
        /*
         * Provided buffer is too short.
         * Return the expected size together with status "short buffer"
         */
        EMSG("Provided buffer is too short, expected size=%d", object_info.dataSize);
        TEE_CloseObject(object);
        TEE_Free(id);
        return 0;
    }

    //printf("calling TEE_ReadObjectData\n");
    res = TEE_ReadObjectData(object, data, object_info.dataSize,
                             &read_bytes);
    if (res != TEE_SUCCESS || read_bytes != object_info.dataSize) {
        EMSG("TEE_ReadObjectData failed 0x%08x, read %" PRIu32 " over %u",
                res, read_bytes, object_info.dataSize);
        TEE_CloseObject(object);
        TEE_Free(id);
        return 0;
    }

    TEE_CloseObject(object);
    TEE_Free(id);
    return 1;
}