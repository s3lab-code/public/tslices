#include "im2col_TA.h"
#include <stdio.h>
#include "math_TA.h"

float im2col_get_pixel_TA(float *im, int height, int width, int channels,
                          int row, int col, int channel, int pad, int im_offset)
{
    row -= pad;
    col -= pad;

    if (row < 0 || col < 0 || row >= height || col >= width) {
        return 0;
    }
    return im[im_offset + col + width*(row + height*channel)];
}

//From Berkeley Vision's Caffe!
//https://github.com/BVLC/caffe/blob/master/LICENSE
void im2col_cpu_TA(float* data_im,
                   int channels,  int height,  int width,
                   int ksize,  int stride, int pad, float* data_col)
{
    //printf("** im2col_cpu_TA\n");
    int c,h,w;
    int height_col = (height + 2*pad - ksize) / stride + 1;
    int width_col = (width + 2*pad - ksize) / stride + 1;

    int channels_col = channels * ksize * ksize;
    for (c = 0; c < channels_col; ++c) {
        int w_offset = c % ksize;
        int h_offset = (c / ksize) % ksize;
        int c_im = c / ksize / ksize;
        for (h = 0; h < height_col; ++h) {
            for (w = 0; w < width_col; ++w) {
                int im_row = h_offset + h * stride;
                int im_col = w_offset + w * stride;
                int col_index = (c * height_col + h) * width_col + w;
                data_col[col_index] = im2col_get_pixel_TA(data_im, height, width, channels,
                                                          im_row, im_col, c_im, pad, 0);
            }
        }
    }
    //printf("** im2col done\n");
}


void im2col_cpu_TA_per_channel(float* data_im,
                               int channels,  int height,  int width,
                               int ksize,  int stride, int pad, float* data_col, int data_im_offset)
{
    int c,h,w;
    int height_col = (height + 2*pad - ksize) / stride + 1;
    int width_col = (width + 2*pad - ksize) / stride + 1;
    //printf("** im2col_cpu_TA_per_channel, channels=%d, stride=%d, pad=%d, height_col=%d, width_col=%d \n", channels, stride, pad, height_col,width_col);

    int channels_col = ksize * ksize * channels;
    //printf("%d, %d, %d\n", height_col, width_col, channels_col);

    for (c = 0; c < channels_col; ++c) {
        int w_offset = c % ksize;
        int h_offset = (c / ksize) % ksize;
        int c_im = c / ksize / ksize;
        for (h = 0; h < height_col; ++h) {
            int im_row = h_offset + h * stride;
            for (w = 0; w < width_col; ++w) {
                int im_col = w_offset + w * stride;
                int col_index = (c * height_col + h) * width_col + w;
                data_col[col_index] = im2col_get_pixel_TA(data_im, height, width, channels,
                                              im_row, im_col, c_im, pad, data_im_offset);
            }
        }
    }
    //printf("\n ** im2col done\n");
}