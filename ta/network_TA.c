#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "darknet_TA.h"
#include "blas_TA.h"
#include "network_TA.h"
#include "math_TA.h"

#include "main_ta.h"
#include <tee_internal_api.h>
#include <tee_internal_api_extensions.h>
#include "memory_management_TA.h"
#include "user_ta_header_defines.h"


network_TA netta;
int roundnum = 0;
float err_sum = 0;
float avg_loss = -1;

float *ta_net_input;
float *ta_net_delta;
float *ta_net_output;

void make_network_TA(int n, float learning_rate, float momentum, float decay, int time_steps, int notruth, int batch, int subdivisions, int random, int adam, float B1, float B2, float eps, int h, int w, int c, int inputs, int max_crop, int min_crop, float max_ratio, float min_ratio, int center, float clip, float angle, float aspect, float saturation, float exposure, float hue, int burn_in, float power, int max_batches, int is_train, int is_encryption_enabled, int is_sublayer_partition_enabled, int is_debug, int is_benchmark)
{
    netta.n = n;

    netta.seen = calloc(1, sizeof(size_t));
    netta.layers = calloc(netta.n, sizeof(layer_TA));
    netta.t    = calloc(1, sizeof(int));
    netta.cost = calloc(1, sizeof(float));

    netta.learning_rate = learning_rate;
    netta.momentum = momentum;
    netta.decay = decay;
    netta.time_steps = time_steps;
    netta.notruth = notruth;
    netta.batch = batch;
    netta.subdivisions = subdivisions;
    netta.random = random;
    netta.adam = adam;
    netta.B1 = B1;
    netta.B2 = B2;
    netta.eps = eps;
    netta.h = h;
    netta.w = w;
    netta.c = c;
    netta.inputs = inputs;
    netta.max_crop = max_crop;
    netta.min_crop = min_crop;
    netta.max_ratio = max_ratio;
    netta.min_ratio = min_ratio;
    netta.center = center;
    netta.clip = clip;
    netta.angle = angle;
    netta.aspect = aspect;
    netta.saturation = saturation;
    netta.exposure = exposure;
    netta.hue = hue;
    netta.burn_in = burn_in;
    netta.power = power;
    netta.max_batches = max_batches;

    netta.train = is_train;
    netta.is_encryption_enabled = is_encryption_enabled;
    netta.is_sublayer_partition_enabled = is_sublayer_partition_enabled;
    netta.is_debug = is_debug;
    netta.is_benchmark = is_benchmark;
    netta.fwd_layer_exec_time = 0;
    netta.gemm_time = calloc(1, sizeof(int));
    netta.im2col_time = calloc(1, sizeof(int));

    if (netta.is_debug) printf("make_network_TA:: netta.n=%d, sizeof(layer_TA)=%d, sizeof(network_TA)=%d \n", netta.n, sizeof(layer_TA), sizeof(network_TA));
    if (netta.is_debug) printf("TA_DATA_SIZE=%d\n", TA_DATA_SIZE);
    //netta.truth = net->truth; ////// ing network.c train_network
}

void forward_network_TA()
{
    roundnum++;
    int i;
    for(i = 0; i < netta.n; ++i){
        netta.index = i;
        layer_TA l = netta.layers[i];

        if(l.delta){
            fill_cpu_TA(l.outputs * l.batch, 0, l.delta, 1);
        }
        printf("Layer: i=%d\n", i);
        l.forward_TA(&l, &netta);

        netta.input = l.output;

        if(l.truth) {
            netta.truth = l.output;
        }
        //output of the network (for predict)
        if(l.type == SOFTMAX_TA){
            ta_net_output = malloc(sizeof(float)*l.outputs*1);
            for(int z=0; z<l.outputs*1; z++){
                ta_net_output[z] = l.output[z];
            }
        }
    }
    calc_network_cost_TA();
}

void forward_network_TA_Temp()
{
    if (netta.is_debug) DMSG("has been called");

    TEE_Time time1, time2, result;
    if (netta.is_benchmark){
        TEE_GetSystemTime(&time1);
    }

    roundnum++;
    int i = netta.layer_index;
    layer_TA l = netta.layers[i];

    if (netta.is_debug) printf("******* TA network layer:: i = %d *******\n", i);

    if(l.type == CONVOLUTIONAL_TA || l.type == CONNECTED_TA){
        l.channel_index += netta.channel_index;
        l.num_merged_sublayers = netta.channel_index;
        if (netta.is_debug) printf("l.num_merged_sublayers=%d, l.channel_index=%d\n", l.num_merged_sublayers, l.channel_index);

        l.forward_TA(&l, &netta);

        //after all sublayer calculation is finished, the output will be next layer's input
        if((l.type == CONVOLUTIONAL_TA && l.channel_index == l.c) || (l.type == CONNECTED_TA && (l.num_merged_sublayers == -1 || l.channel_index == l.outputs))){
            if (netta.input != NULL) {
                free(netta.input);
            }
            netta.input = l.output;
            deallocate_memory_for_layer(&l);
        }
    }
    else{
        l.forward_TA(&l, &netta);

        if(l.type != DROPOUT_TA){
            if (netta.input != NULL) {
                free(netta.input);
            }
            netta.input = l.output;
        }
        deallocate_memory_for_layer(&l);
    }

    if(l.truth) {
        netta.truth = l.output;
    }

    netta.layers[i] = l;

    if (netta.is_debug) IMSG("forward_TA done!\n");

    //output of the network (for prediction)
    if(!netta.train && l.type == SOFTMAX_TA){
        ta_net_output = malloc(sizeof(float)*l.outputs*1);
        for(int z=0; z<l.outputs*1; z++){
            ta_net_output[z] = l.output[z];
        }
    }

    if (netta.is_benchmark){
        TEE_GetSystemTime(&time2);
        TEE_TIME_SUB(time2, time1, result);
        netta.fwd_layer_exec_time += result.seconds*1000 + result.millis;
    }
}


void update_network_TA(update_args_TA a)
{
    int i;
    for(i = 0; i < netta.n; ++i){
        layer_TA l = netta.layers[i];
        if(l.update_TA){
            l.update_TA(l, a);
        }
    }
}


void calc_network_cost_TA()
{
    int i;
    float sum = 0;
    int count = 0;
    for(i = 0; i < netta.n; ++i){
        if(netta.layers[i].cost){
            sum += netta.layers[i].cost[0];
            ++count;
        }
    }
    *netta.cost = sum/count;
    err_sum += *netta.cost;
}


void calc_network_loss_TA(int n, int batch)
{
    float loss = (float)err_sum/(n*batch);

    if(avg_loss == -1) avg_loss = loss;
    avg_loss = avg_loss*.9 + loss*.1;

    char loss_char[20];
    char avg_loss_char[20];
    ftoa(loss, loss_char, 5);
    ftoa(avg_loss, avg_loss_char, 5);
    IMSG("loss = %s, avg loss = %s from the TA\n",loss_char, avg_loss_char);
    err_sum = 0;
}



void backward_network_TA(float *ca_net_input, float *ca_net_delta)
{
    int i;

    for(i = netta.n-1; i >= 0; --i){
        layer_TA l = netta.layers[i];

        if(l.stopbackward) break;
        if(i == 0){
            // ta_net_input malloc so not destroy before addition backward
            ta_net_input = malloc(sizeof(float)*l.inputs*l.batch);
            ta_net_delta = malloc(sizeof(float)*l.inputs*l.batch);

            for(int z=0; z<l.inputs*l.batch; z++){
                ta_net_input[z] = ca_net_input[z];
                ta_net_delta[z] = ca_net_delta[z];
            }

            netta.input = ta_net_input;
            netta.delta = ta_net_delta;
        }else{
            layer_TA prev = netta.layers[i-1];
            netta.input = prev.output;
            netta.delta = prev.delta;
        }

        netta.index = i;
        l.backward_TA(l, netta);

        if((l.type == DROPOUT_TA) && (i == 0)){
            for(int z=0; z<l.inputs*l.batch; z++){
                ta_net_input[z] = l.output[z];
                ta_net_delta[z] = l.delta[z];
            }
            //netta.input = l.output;
            //netta.delta = l.delta;
        }
    }
    //backward_network_back_TA_params(netta.input, netta.delta, netta.layers[0].inputs, netta.batch);
}
